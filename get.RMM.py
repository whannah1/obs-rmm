#!/usr/bin/env python
#===========================================================================================
# Simple script to retrieve RMM data From Matt Wheeler's Website
#
#    Mar, 2013	Walter Hannah 		Colorado State University
#===========================================================================================
from ecmwfapi import ECMWFDataServer
import sys
import os
import numpy as np
#===========================================================================================
#===========================================================================================

SRC = "http://cawcr.gov.au/staff/mwheeler/maproom/RMM/RMM1RMM2.74toRealtime.txt"
DST = "~/Research/OBS/RMM/RMM.74toCurrent.txt"

cmd = "wget -N "+SRC+" -O "+DST

os.system(cmd)

#===========================================================================================
#===========================================================================================
